﻿using System;

namespace FourFort.Attributes {
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CommandAttribute : Attribute {
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }

        public bool IsEqual(string command, bool isCaseSensitive) {
            return isCaseSensitive
                ? command == Name || command == Alias
                : command.Equals(Name, StringComparison.OrdinalIgnoreCase) ||
                  command.Equals(Alias, StringComparison.OrdinalIgnoreCase);
        }
    }
}