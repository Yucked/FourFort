﻿using System;

namespace FourFort.Attributes {
    public sealed class AskQuestionAttribute : Attribute {
        public string Question { get; }
        public bool RequiresBinaryAnswer { get; }

        public AskQuestionAttribute(string question, bool requiresBinaryAnswer = false) {
            Question = question;
            RequiresBinaryAnswer = requiresBinaryAnswer;
        }
    }
}