﻿using System;

namespace FourFort.Attributes {
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class InjectAttribute : Attribute { }
}