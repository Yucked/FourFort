using System.IO;
using System.IO.Compression;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using FourFort.Entities;
using Microsoft.Extensions.Logging;

namespace FourFort.Managers {
    public sealed class VerificationManager {
        private readonly ILogger _logger;
        private readonly ProcessManager _processManager;
        private readonly HttpClient _httpClient;
        private readonly ApplicationOptions _applicationOptions;
        private const string STEAM_CMD_URL = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip";

        public VerificationManager(ILogger<VerificationManager> logger, IHttpClientFactory clientFactory,
                                   ApplicationOptions applicationOptions, ProcessManager processManager) {
            _logger = logger;
            _httpClient = clientFactory.CreateClient();
            _applicationOptions = applicationOptions;
            _processManager = processManager;
        }

        public async Task VerifyStartupAsync() {
            _logger.LogInformation("Verifying SteamCMD and Team Fortress 2 installations.");
            VerifyDirectories();
            await VerifySteamAsync();
            VerifyServer();
            _logger.LogInformation("Verification completed!");
        }

        private void VerifyDirectories() {
            var optionsChanged = false;

            if (string.IsNullOrWhiteSpace(_applicationOptions.SteamCMDPath) ||
                !Directory.Exists(_applicationOptions.SteamCMDPath)) {
                _logger.LogWarning("Steam CMD directory doesn't exist.");

                var directory = CreateDirectory("SteamCMD");
                _applicationOptions.SteamCMDPath = directory;

                _logger.LogInformation($"Created Steam CMD directory: {directory}");
                optionsChanged = true;
            }

            if (string.IsNullOrWhiteSpace(_applicationOptions.ServerPath) ||
                !Directory.Exists(_applicationOptions.ServerPath)) {
                _logger.LogWarning("Team Fortress 2 server directory doesn't exist.");

                var directory = CreateDirectory("TF2S");
                _applicationOptions.ServerPath = directory;

                _logger.LogInformation($"Created Team Fortress 2 server directory: {directory}");
                optionsChanged = true;
            }

            if (!optionsChanged)
                return;

            var serialized = JsonSerializer.SerializeToUtf8Bytes(_applicationOptions);
            File.WriteAllBytes(OptionsManager.FILE_NAME, serialized);
        }

        private async Task VerifySteamAsync() {
            if (File.Exists($@"{_applicationOptions.SteamCMDPath}\steamcmd.exe")) {
                _logger.LogInformation("Steam CMD already exists!");
                return;
            }

            _logger.LogError("Steam CMD not installed.");

            _logger.LogDebug($"Downloading Steam CMD from {STEAM_CMD_URL}");
            var stream = await _httpClient.GetStreamAsync(STEAM_CMD_URL);
            await using var fileStream = new FileStream($@"{_applicationOptions.SteamCMDPath}\steamcmd.zip",
                FileMode.CreateNew);

            await stream.CopyToAsync(fileStream);
            await stream.FlushAsync();

            stream.Close();
            fileStream.Close();
            _logger.LogDebug("Steam CMD downloaded!");

            _logger.LogDebug($"Unzipping Steam CMD to {_applicationOptions.SteamCMDPath}");
            ZipFile.ExtractToDirectory($@"{_applicationOptions.SteamCMDPath}\steamcmd.zip",
                _applicationOptions.SteamCMDPath);

            _logger.LogDebug($"Finished unzipping Steam CMD to {_applicationOptions.SteamCMDPath}");
            _logger.LogInformation($"Steam CMD installed at {_applicationOptions.SteamCMDPath}");

            _processManager.StartProcess($@"{_applicationOptions.SteamCMDPath}\steamcmd.exe");
        }

        private void VerifyServer() {
            if (File.Exists($@"{_applicationOptions.ServerPath}\srcds.exe")) {
                return;
            }

            _logger.LogError("Team Fortress 2 server not installed.");
        }

        private static string CreateDirectory(string directoryName) {
            return Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), directoryName)).FullName;
        }
    }
}