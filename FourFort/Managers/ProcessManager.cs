using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using Microsoft.Extensions.Logging;

namespace FourFort.Managers {
    public sealed class ProcessManager {
        public CancellationTokenSource ExitToken { get; }
        public ConcurrentDictionary<string, (Process Process, ILogger Logger)> Processes { get; }
        private readonly ILogger _logger;
        private readonly ILoggerFactory _loggerFactory;

        public ProcessManager(ILogger<ProcessManager> logger, ILoggerFactory loggerFactory) {
            _logger = logger;
            _loggerFactory = loggerFactory;
            ExitToken = new CancellationTokenSource();
            Processes = new ConcurrentDictionary<string, (Process Process, ILogger Logger)>();
        }

        public void StartProcess(string path) {
            var processStartInfo = new ProcessStartInfo(path) {
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            var process = Process.Start(processStartInfo);
            process.Exited += OnExited;
            process.ErrorDataReceived += OnErrorDataReceived;
            process.OutputDataReceived += OnOutputDataReceived;

            process.BeginErrorReadLine();
            process.BeginOutputReadLine();

            var logger = _loggerFactory.CreateLogger(process.ProcessName);
            Processes.TryAdd(process.ProcessName, (process, logger));
            _logger.LogInformation($"Started process {process.ProcessName}.");
        }

        public void CloseProcess(string processName) {
            if (!Processes.TryGetValue(processName, out var info))
                return;

            info.Process.Kill(true);
            _logger.LogWarning($"Closed {processName} process.");
        }

        public void SendCommand(string processName, string command) { }

        private void OnExited(object? sender, EventArgs e) {
            if (!(sender is Process process))
                return;

            _logger.LogWarning($"Process {process.ProcessName} has exited!");
        }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e) {
            if (!(sender is Process process))
                return;

            if (!Processes.TryGetValue(process.ProcessName, out var info))
                return;

            info.Logger.LogError(e.Data);
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e) {
            if (!(sender is Process process))
                return;

            if (!Processes.TryGetValue(process.ProcessName, out var info))
                return;

            info.Logger.LogInformation(e.Data);
        }
    }
}