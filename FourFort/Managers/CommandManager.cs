using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using FourFort.Attributes;
using FourFort.Entities;
using FourFort.Interfaces;
using Microsoft.Extensions.Logging;

namespace FourFort.Managers {
    public sealed class CommandManager {
        public ConcurrentDictionary<CommandAttribute, ICommand> Commands { get; }
        private readonly ApplicationOptions _applicationOptions;
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;

        public CommandManager(ApplicationOptions applicationOptions, ILogger<CommandManager> logger,
                              IServiceProvider serviceProvider) {
            _applicationOptions = applicationOptions;
            _logger = logger;
            _serviceProvider = serviceProvider;
            Commands = new ConcurrentDictionary<CommandAttribute, ICommand>();
        }

        public void AddCommands() {
            var commands = typeof(Program).Assembly.GetTypes()
                .Where(x => typeof(ICommand).IsAssignableFrom(x) && !x.IsInterface).ToArray();

            _logger.LogInformation($"Discovered {commands.Length} commands.");
            foreach (var typeCommand in commands) {
                var commandAttribute = typeCommand.GetCustomAttribute<CommandAttribute>();
                var commandInstance = Activator.CreateInstance(typeCommand);

                var properties = commandInstance.GetType().GetProperties();
                foreach (var property in properties) {
                    var injectAttribute = property.GetCustomAttribute<InjectAttribute>();
                    if (injectAttribute == null)
                        continue;

                    var propertyInstance = _serviceProvider.GetService(property.PropertyType);
                    if (propertyInstance == null)
                        continue;

                    property.SetValue(commandInstance, propertyInstance);
                }

                Commands.TryAdd(commandAttribute, commandInstance as ICommand);
                _logger.LogDebug($"Added {commandAttribute.Name} command.");
            }
        }

        public CommandAttribute GetCommandInfo(string command) {
            CommandAttribute commandAttribute = null;
            foreach (var (key, _) in Commands) {
                if (!key.IsEqual(command, _applicationOptions.CaseSensitive))
                    continue;

                commandAttribute = key;
                break;
            }

            return commandAttribute;
        }

        public void Execute(string command, string argument) {
            foreach (var (key, value) in Commands) {
                if (!key.IsEqual(command, _applicationOptions.CaseSensitive))
                    continue;

                value.Execute(argument);
            }
        }
    }
}