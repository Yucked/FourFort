using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;
using FourFort.Attributes;
using FourFort.Entities;
using Microsoft.Extensions.Logging;

namespace FourFort.Managers {
    public readonly struct OptionsManager {
        public const string FILE_NAME = "options.json";

        public static bool IsCreated
            => File.Exists(FILE_NAME);

        public static ApplicationOptions BuildConfiguration() {
            Extensions.WriteLine<OptionsManager>(LogLevel.Warning,
                "First time setup detected! Let's set everything up!");

            var options = new ApplicationOptions {
                Logging = new LoggingOptions {
                    DefaultLevel = LogLevel.Trace,
                    Filters = new Dictionary<string, LogLevel> {
                        {"System", LogLevel.Trace},
                        {"Microsoft", LogLevel.Trace}
                    }
                }
            };

            var properties = options.GetType().GetProperties();
            foreach (var property in properties) {
                var questionAttribute = property.GetCustomAttribute<AskQuestionAttribute>();
                if (questionAttribute == null)
                    continue;

                Extensions.WriteLine<OptionsManager>(LogLevel.Debug,
                    questionAttribute.RequiresBinaryAnswer
                        ? $"{questionAttribute.Question} (Y/N)"
                        : questionAttribute.Question);

                var input = Extensions.ReadInput(">>");
                if (questionAttribute.RequiresBinaryAnswer) {
                    property.SetValue(options, input.ToLower() == "y");
                }
                else {
                    property.SetValue(options, input);
                }
            }

            var serialized = JsonSerializer.SerializeToUtf8Bytes(options);
            File.WriteAllBytes(FILE_NAME, serialized);
            return options;
        }

        public static ApplicationOptions LoadConfiguration() {
            var dataBytes = File.ReadAllBytes(FILE_NAME);
            var applicationOptions = JsonSerializer.Deserialize<ApplicationOptions>(dataBytes);

            Extensions.WriteLine<OptionsManager>(LogLevel.Information,
                "Loading application options! Ready to rock n roll!");

            return applicationOptions;
        }
    }
}