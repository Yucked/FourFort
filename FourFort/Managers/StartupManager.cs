﻿using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using Colorful;
using FourFort.Entities;
using Microsoft.Extensions.Logging;

namespace FourFort.Managers {
    public sealed class StartupManager {
        private readonly ApplicationOptions _applicationOptions;
        private readonly CommandManager _commandManager;
        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;
        private readonly ProcessManager _processManager;
        private readonly VerificationManager _verificationManager;

        public StartupManager(IHttpClientFactory clientFactory, ILogger<StartupManager> logger,
                              ProcessManager processManager, ApplicationOptions applicationOptions,
                              CommandManager commandManager, VerificationManager verificationManager) {
            _httpClient = clientFactory.CreateClient();
            _processManager = processManager;
            _applicationOptions = applicationOptions;
            _commandManager = commandManager;
            _verificationManager = verificationManager;
            _logger = logger;
        }

        public async Task InitializeAsync() {
            _commandManager.AddCommands();

            if (_applicationOptions.VerifyOnStartup) {
                await _verificationManager.VerifyStartupAsync();
            }

            await Task.Delay(-1);
        }

        private async Task BeginReadAsync() {
            await Task.Delay(500, _processManager.ExitToken.Token);
            while (!_processManager.ExitToken.IsCancellationRequested) {
                var readInput = Extensions.ReadInput(prefixColor: Color.LimeGreen);
                var splitInput = readInput.Split(" ");

                if (splitInput.Length == 1)
                    _commandManager.Execute(readInput, string.Empty);
                else
                    _commandManager.Execute(splitInput[0], splitInput[1]);

                Console.ResetColor();
                Console.ForegroundColor = Color.White;
            }
        }
    }
}