﻿using System;
using FourFort.Attributes;
using FourFort.Interfaces;
using FourFort.Managers;

namespace FourFort.Commands {
    [Command(Name = "Exit", Alias = "Ext",
        Description = "Exits the application or child process e.g steamcmd/scrds.exe.")]
    public sealed class ExitCommand : ICommand {
        [Inject]
        public ProcessManager ProcessManager { get; set; }

        public void Execute(string argument = null) {
            foreach (var (name, _) in ProcessManager.Processes) ProcessManager.CloseProcess(name);

            ProcessManager.ExitToken.Cancel(false);
            Console.ReadKey();
            Environment.Exit(-1);
        }
    }
}