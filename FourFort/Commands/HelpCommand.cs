﻿using System.Drawing;
using Colorful;
using FourFort.Attributes;
using FourFort.Interfaces;
using FourFort.Managers;

namespace FourFort.Commands {
    [Command(Name = "Help", Alias = "h", Description = "Shows information about a command or shows all commands.")]
    public sealed class HelpCommand : ICommand {
        private const string FORMAT = "{0}, {1}\n    - {2}";

        [Inject]
        public CommandManager CommandManager { get; set; }

        public void Execute(string argument = null) {
            if (!string.IsNullOrWhiteSpace(argument)) {
                var commandInfo = CommandManager.GetCommandInfo(argument);
                WriteCommandInfo(commandInfo);
            }
            else {
                foreach (var (key, _) in CommandManager.Commands) WriteCommandInfo(key);
            }
        }

        private void WriteCommandInfo(CommandAttribute commandInfo) {
            var formatters = new[] {
                new Formatter(commandInfo.Name, Color.Gold),
                new Formatter(commandInfo.Alias, Color.Gold),
                new Formatter(commandInfo.Description, Color.DimGray)
            };

            Console.WriteLineFormatted(FORMAT, Color.White, formatters);
        }
    }
}