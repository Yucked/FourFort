﻿using System;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using Colorful;
using Microsoft.Extensions.Logging;
using Console = Colorful.Console;

namespace FourFort {
    internal static class Extensions {
        public static Color GetLogColor(this LogLevel logLevel) {
            return logLevel switch {
                LogLevel.Information => Color.SpringGreen,
                LogLevel.Debug       => Color.Coral,
                LogLevel.Trace       => Color.MediumPurple,
                LogLevel.Critical    => Color.Red,
                LogLevel.Error       => Color.Crimson,
                LogLevel.Warning     => Color.Orange,
                _                    => Color.Tomato
            };
        }

        public static string PadBoth(this string source, int length) {
            var spaces = length - source.Length;
            var padLeft = spaces / 2 + source.Length;
            return source.PadLeft(padLeft).PadRight(length);
        }

        public static void PrintHeader() {
            const string LOGO = @"
                $$$$$$$$\                            $$$$$$$$\                   $$\     
                $$  _____|                           $$  _____|                  $$ |    
                $$ |    $$$$$$\  $$\   $$\  $$$$$$\  $$ |    $$$$$$\   $$$$$$\ $$$$$$\   
                $$$$$\ $$  __$$\ $$ |  $$ |$$  __$$\ $$$$$\ $$  __$$\ $$  __$$\\_$$  _|  
                $$  __|$$ /  $$ |$$ |  $$ |$$ |  \__|$$  __|$$ /  $$ |$$ |  \__| $$ |    
                $$ |   $$ |  $$ |$$ |  $$ |$$ |      $$ |   $$ |  $$ |$$ |       $$ |$$\ 
                $$ |   \$$$$$$  |\$$$$$$  |$$ |      $$ |   \$$$$$$  |$$ |       \$$$$  |
                \__|    \______/  \______/ \__|      \__|    \______/ \__|        \____/ 
";

            Console.WriteLine(LOGO, Color.Crimson);

            const string LOG_MESSAGE = "                                    Version: {0}\n" +
                                       "    Framework: {1} - OS Arch: {2} - Process Arch: {3} - OS: {4}";

            var informationalVersionAttribute =
                typeof(Program).Assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();

            var formatters = new[] {
                new Formatter(informationalVersionAttribute.InformationalVersion.Trim(), Color.Gold),
                new Formatter(RuntimeInformation.FrameworkDescription, Color.Aqua),
                new Formatter(RuntimeInformation.OSArchitecture, Color.Gold),
                new Formatter(RuntimeInformation.ProcessArchitecture, Color.LawnGreen),
                new Formatter(RuntimeInformation.OSDescription, Color.HotPink)
            };

            Console.WriteLineFormatted(LOG_MESSAGE, Color.White, formatters);
            Console.Title = $"FourFort - v{informationalVersionAttribute.InformationalVersion.Trim()}";

            Console.WriteLine(new string('-', 105), Color.Gray);
        }

        public static string ReadInput(string prefix = ">", Color? prefixColor = default) {
            Console.Write($"{prefix} ", prefixColor ?? Color.HotPink);

            var readInput = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(readInput)) return readInput;

            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            return string.Empty;
        }

        public static void WriteLine<T>(LogLevel logLevel, string message = null, Exception exception = null) {
            var messageToWrite = message ?? exception.StackTrace;
            if (string.IsNullOrWhiteSpace(messageToWrite))
                return;

            const string FORMAT = "[{0}] [{1}] [{2}]\n    {3}";
            var date = DateTimeOffset.Now;
            var color = logLevel.GetLogColor();

            var formatters = new[] {
                new Formatter($"{date:MMM d - hh:mm:ss tt}", Color.Gray),
                new Formatter(Enum.GetName(typeof(LogLevel), logLevel).PadBoth(15), color),
                new Formatter(typeof(T).FullName, Color.Gold),
                new Formatter(messageToWrite, Color.White)
            };

            Console.WriteLineFormatted(FORMAT, Color.White, formatters);
        }

        public static void PrintIntroMessage() { }
    }
}