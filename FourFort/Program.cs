using System.Threading.Tasks;
using FourFort.Logging;
using FourFort.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FourFort {
    internal sealed class Program {
        private static async Task Main() {
            Extensions.PrintHeader();
            var applicationOptions = OptionsManager.IsCreated
                ? OptionsManager.LoadConfiguration()
                : OptionsManager.BuildConfiguration();

            var serviceCollection = new ServiceCollection()
                .AddSingleton(applicationOptions)
                .AddSingleton<CommandManager>()
                .AddSingleton<ProcessManager>()
                .AddSingleton<StartupManager>()
                .AddSingleton<VerificationManager>()
                .AddHttpClient()
                .AddLogging(x => {
                    x.ClearProviders();
                    x.AddProvider(new LoggerProvider(applicationOptions));
                });

            var provider = serviceCollection.BuildServiceProvider();
            var startupManager = provider.GetRequiredService<StartupManager>();
            await startupManager.InitializeAsync();
        }
    }
}