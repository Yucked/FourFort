﻿namespace FourFort.Interfaces {
    public interface ICommand {
        void Execute(string argument = null);
    }
}