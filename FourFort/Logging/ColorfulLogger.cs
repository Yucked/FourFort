using System;
using System.Drawing;
using Colorful;
using Microsoft.Extensions.Logging;

namespace FourFort.Logging {
    public readonly struct ColorfulLogger : ILogger {
        private readonly string _categoryName;
        private readonly LogLevel _categoryLevel;
        private readonly LoggerProvider _provider;

        public ColorfulLogger(string categoryName, LogLevel categoryLevel, LoggerProvider provider) {
            _categoryName = categoryName;
            _categoryLevel = categoryLevel;
            _provider = provider;
        }

        public IDisposable BeginScope<TState>(TState state) {
            return default;
        }

        public bool IsEnabled(LogLevel logLevel) {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
                                Func<TState, Exception, string> formatter) {
            if (!IsEnabled(logLevel))
                return;

            var message = formatter(state, exception);
            if (string.IsNullOrWhiteSpace(message))
                return;

            var date = DateTimeOffset.Now;
            var color = logLevel.GetLogColor();

            var formatters = new[] {
                new Formatter($"{date:MMM d - hh:mm:ss tt}", Color.Gray),
                new Formatter(Enum.GetName(typeof(LogLevel), logLevel).PadBoth(15), color),
                new Formatter(_categoryName, Color.Gold),
                new Formatter(message, Color.White)
            };

            _provider.Enqueue(formatters);
        }
    }
}