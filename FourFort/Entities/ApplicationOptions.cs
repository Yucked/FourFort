﻿using System.Text.Json.Serialization;
using FourFort.Attributes;

namespace FourFort.Entities {
    public sealed class ApplicationOptions {
        [AskQuestion("Do you want commands to be case sensitive?", true)]
        public bool CaseSensitive { get; set; }

        [AskQuestion("Do you want to verify files on startup?", true)]
        public bool VerifyOnStartup { get; set; }

        [JsonPropertyName("steamCMDPath")]
        [AskQuestion("If SteamCMD is installed, please enter it's path.")]
        public string SteamCMDPath { get; set; }

        [AskQuestion("If Team Fortress 2 server is already installed, please enter it's path.")]
        public string ServerPath { get; set; }

        [JsonPropertyName("logging_options")]
        public LoggingOptions Logging { get; set; }
    }
}