﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;

namespace FourFort.Entities {
    public class LoggingOptions {
        [JsonPropertyName("default_level")]
        public LogLevel DefaultLevel { get; set; }

        [JsonPropertyName("log_filter")]
        public IReadOnlyDictionary<string, LogLevel> Filters { get; set; }
    }
}