﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace FourFort.Entities {
    public class PlayerInfo {
        public string SteamId { get; set; }
        public string CurrentUsername { get; set; }
        public string IpAddress { get; set; }
        public List<string> IpAddresses { get; set; }
        public ConcurrentDictionary<string, DateTimeOffset> Usernames { get; set; }
        public ConcurrentDictionary<string, int> Kills { get; set; }
        public int Revenges { get; set; }
        public int Dominations { get; set; }
    }
}